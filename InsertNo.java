package mar_21Test;

import java.util.ArrayList;
import java.util.Arrays;

public class InsertNo {

		public static void main(String[] args) {  
			 
			Integer arr[] = {2,9,5,0};  
			
			System.out.println("Array:"+Arrays.toString(arr));  
			
			ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(arr)); 
			
			arrayList.add(4);  
			
			arr = arrayList.toArray(arr);  
			
			System.out.println("After array adding element: "+Arrays.toString(arr));  
			  
			}  
			  
			  
			}

